import React, { Component } from 'react'

export default class Footer extends Component {
	render() {
		return (
			<div className="footer ">
				<p className="clearfix lighten-2 text-sm-center mb-0 px-2">
					<span className="float-md-left d-block d-md-inline-block">Copyright © 2019 All rights reserved.</span>
					<span className="float-md-right d-block d-md-inline-blockd-none d-lg-block">TRUTH TRAINING</span>
				</p>
			</div>
		)
	}
}
