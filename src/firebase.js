import firebase from 'firebase'

var config = {
	apiKey: 'AIzaSyBSzfx5GKDIkwd8mdpQmo0Gtz5yZWNv2Ms',
	authDomain: 'truth-shop-13cab.firebaseapp.com',
	databaseURL: 'https://truth-shop-13cab.firebaseio.com',
	projectId: 'truth-shop-13cab',
	storageBucket: 'truth-shop-13cab.appspot.com',
	messagingSenderId: '78748993510'
}
firebase.initializeApp(config)

export const database = firebase.database()
export const auth = firebase.auth()
export const storage = firebase.storage()